package de.helmholtz.cloud.cerebrum.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.lang.Nullable;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

import de.helmholtz.cloud.cerebrum.annotation.ForeignKey;
import de.helmholtz.cloud.cerebrum.utils.CerebrumEntityUuidGenerator;

import static de.helmholtz.cloud.cerebrum.utils.CerebrumEntityUuidGenerator.generate;

@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
@Document
public class MarketService extends AuditMetadata
{
    @Schema(description = "Unique identifier of the market service.",
            example = "svc-01eac6d7-0d35-1812-a3ed-24aec4231940", required = true)
    @Setter(AccessLevel.NONE)
    @Id
    private String uuid = generate("svc");

    @NotNull
    @Schema(description = "lowercase name of a service (needed for sorting)", example = "sync+share", required = true)
    private String name;

    @NotNull
    @Schema(description = "Name of a Service to display", example = "Sync+Share", required = true)
    private String displayName;

    @Schema(description = "Description of a Service",
            example = "A awesome Sync+Share Service provides by Helmholtz Zentrum xy")
    private String description;

    @Schema(description = "Summary of the service's description", example = "Sync+Share Service")
    private String summary;

    @Schema(description = "Url to a Service", example = "serviceXy.helmholtz.de")
    private String entryPoint;

    @Schema(description = "The service version number", example = "1.0.1")
    private String version;

    @Schema(description = "Service's email address", example = "fake-email@example.org")
    @Email
    private String email;

    @Schema(description = "", example = "True")
    private boolean multiTenancy;

    @Schema(description = "")
    private String enrolmentPolicy;

    @Schema(description = "")
    private String policy;

    @Schema(description = "")
    private String documentation;

    @Schema(description = "", example = "pht-01eac6d7-0d35-1812-a3ed-24aec4231940")
    @ForeignKey
    private String logoId;

    @Schema(description = "", example = "PRODUCTION")
    private Phase phase;

    @Schema(description = "")
    private Set<String> targetGroup = new TreeSet<>();

    @Schema(description = "")
    private Set<String> tags = new TreeSet<>();

    @Schema(description = "Organisation providing this service")
    private Organization serviceProvider;

    @Schema(description = "")
    private Set<String> managementTeam = new TreeSet<>();

    @Schema(description = "Name of software used for sorting")
    private String software;

    @Schema(description = "list of software that this service is based on or extended")
    private Set<Software> softwareList = new TreeSet<>();

    public void setUuid(@Nullable String uuid)
    {
        this.uuid =  Boolean.TRUE.equals(
                CerebrumEntityUuidGenerator.isValid(uuid))
                ? uuid : generate("svc");
    }

    public void addTarget(String target)
    {
        targetGroup.add(target);
    }

    public void removeTarget(String target)
    {
        targetGroup.remove(target);
    }

    public void addTag(String tag)
    {
        tags.add(tag);
    }

    public void removeTag(String tag)
    {
        tags.remove(tag);
    }

    public void addTeamMember(String member)
    {
        CerebrumEntityUuidGenerator.matchUuidWithClass(member, MarketUser.class);
        managementTeam.add(member);
    }

    public void removeTeamMember(String member)
    {
        managementTeam.remove(member);
    }

    public void addSoftware(Software sft)
    {
        softwareList.add(sft);
    }

    public void removeSoftware(Software sft)
    {
        softwareList.remove(sft);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MarketService service = (MarketService) o;
        return name.equals(service.name) &&
                entryPoint.equals(service.entryPoint);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(name, entryPoint);
    }
}

enum Phase
{
    TEST,
    PILOT,
    PRODUCTION
}
