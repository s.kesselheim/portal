package de.helmholtz.cloud.cerebrum.service;

import com.github.fge.jsonpatch.JsonPatch;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import de.helmholtz.cloud.cerebrum.entity.Person;
import de.helmholtz.cloud.cerebrum.repository.PersonRepository;
import de.helmholtz.cloud.cerebrum.service.common.CerebrumServiceBase;
import de.helmholtz.cloud.cerebrum.service.common.ForeignKeyExecutorService;

@Service
public class PersonService extends CerebrumServiceBase<Person, PersonRepository, ForeignKeyExecutorService>
{
    private final PersonRepository personRepository;
    private final ForeignKeyExecutorService foreignKeyExecutorService;

    protected PersonService(PersonRepository personRepository,
                            ForeignKeyExecutorService foreignKeyExecutorService)
    {
        super(Person.class, PersonRepository.class, ForeignKeyExecutorService.class);
        this.personRepository = personRepository;
        this.foreignKeyExecutorService = foreignKeyExecutorService;
    }

    public Page<Person> getPersons(PageRequest page)
    {
        return getAllEntities(page, personRepository);
    }

    public Person getPerson(String uuid)
    {
        return getEntity(uuid, personRepository);
    }

    public Person getPerson(String firstName, String lastName)
    {
        return personRepository.findByFirstNameAndLastName(firstName, lastName);
    }

    public Person createPerson(Person entity)
    {
        return createEntity(entity, personRepository, foreignKeyExecutorService);
    }

    public ResponseEntity<Person> createPerson(Person entity, UriComponentsBuilder uriComponentsBuilder)
    {
        return createEntity(entity, personRepository, foreignKeyExecutorService, uriComponentsBuilder);
    }

    public ResponseEntity<Person> updatePerson(
            String uuid, Person entity, UriComponentsBuilder uriComponentsBuilder)
    {
        return updateEntity(uuid, entity, personRepository, foreignKeyExecutorService, uriComponentsBuilder);
    }

    public ResponseEntity<Person> partiallyUpdatePerson(String uuid, JsonPatch patch)
    {
        return partiallyUpdateEntity(uuid, personRepository, foreignKeyExecutorService, patch);
    }

    public ResponseEntity<Person> deletePerson(String uuid)
    {
        return deleteEntity(uuid, personRepository, foreignKeyExecutorService);
    }
}
