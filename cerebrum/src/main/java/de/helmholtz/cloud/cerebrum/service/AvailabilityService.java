package de.helmholtz.cloud.cerebrum.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import de.helmholtz.cloud.cerebrum.entity.Availability;
import de.helmholtz.cloud.cerebrum.repository.AvailabilityRepository;
import de.helmholtz.cloud.cerebrum.service.common.CerebrumServiceBase;
import de.helmholtz.cloud.cerebrum.service.common.ForeignKeyExecutorService;

@Service
public class AvailabilityService extends CerebrumServiceBase<Availability, AvailabilityRepository, ForeignKeyExecutorService>
{
    private final AvailabilityRepository repository;
    private final ForeignKeyExecutorService foreignKeyExecutorService;

    protected AvailabilityService(AvailabilityRepository repository, ForeignKeyExecutorService foreignKeyExecutorService)
    {
        super(Availability.class, AvailabilityRepository.class, ForeignKeyExecutorService.class);
        this.repository = repository;
        this.foreignKeyExecutorService = foreignKeyExecutorService;
    }

    public Page<Availability> getAvailabilities(PageRequest page)
    {
        return getAllEntities(page, repository);
    }

    public Availability getAvailability(String id)
    {
        return getEntity(id, repository);
    }
}
