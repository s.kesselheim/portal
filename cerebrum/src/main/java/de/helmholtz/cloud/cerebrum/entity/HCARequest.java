package de.helmholtz.cloud.cerebrum.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import de.helmholtz.cloud.cerebrum.utils.CerebrumEntityUuidGenerator;
import de.helmholtz.cloud.hca.message.ResourceAllocateV1Schema;
import static de.helmholtz.cloud.cerebrum.utils.CerebrumEntityUuidGenerator.generate;

import javax.validation.constraints.NotNull;

@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
@Document
public class HCARequest extends AuditMetadata {
    @Schema(description = "Unique identifier of the HCA request.", example = "hca-01eac6d7-0d35-1812-a3ed-24aec4231940", required = true)
    @Setter(AccessLevel.NONE)
    @Id
    private String uuid = generate("hca");

    @Version private Long version;
    @NonNull
    private ResourceAllocateV1Schema request;

    @NonNull
    private String status;

    @NotNull
    private String requestId;

    @NotNull
    private String userId;

    @NotNull
    private String serviceName;

    @Nullable
    private String resourceId;

    public void setUuid(@Nullable String uuid) {
        this.uuid = Boolean.TRUE.equals(CerebrumEntityUuidGenerator.isValid(uuid)) ? uuid : generate("hca");
    }

    public HCARequest(ResourceAllocateV1Schema request, String serviceName) {
        this.requestId = CerebrumEntityUuidGenerator.generateType1UUID().toString();
        this.userId = request.getRequester().getId();
        this.request = request;
        this.status = "created";
        this.serviceName = serviceName;
    }
}
