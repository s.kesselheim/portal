package de.helmholtz.cloud.cerebrum.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

import de.helmholtz.cloud.cerebrum.entity.Availability;
import de.helmholtz.cloud.cerebrum.repository.fragment.CerebrumRepository;

public interface AvailabilityRepository extends MongoRepository<Availability, String>, CerebrumRepository<Availability>
{
    Optional<Availability> findById(String id);
}
