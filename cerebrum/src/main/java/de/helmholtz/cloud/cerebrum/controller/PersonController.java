package de.helmholtz.cloud.cerebrum.controller;

import com.github.fge.jsonpatch.JsonPatch;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.List;

import de.helmholtz.cloud.cerebrum.entity.Person;
import de.helmholtz.cloud.cerebrum.errorhandling.CerebrumApiError;
import de.helmholtz.cloud.cerebrum.service.PersonService;
import de.helmholtz.cloud.cerebrum.utils.CerebrumControllerUtilities;

@RestController
@Validated
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE,
        path = "${spring.data.rest.base-path}/persons")
@Tag(name = "persons", description = "The Person API")
public class PersonController
{
    private final PersonService personService;

    public PersonController(PersonService personService)
    {
        this.personService = personService;
    }

    /* get persons */
    @Operation(summary = "get array list of all persons")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation",
                    content = @Content(array = @ArraySchema(
                            schema = @Schema(implementation = Person.class)))),
            @ApiResponse(responseCode = "400", description = "invalid request",
                    content = @Content(array = @ArraySchema(schema = @Schema(implementation = CerebrumApiError.class))))
    })
    @GetMapping(path = "")
    public Iterable<Person> getPersons(
            @Parameter(description = "specify the page number")
            @RequestParam(value = "page", defaultValue = "0") @Min(0) Integer page,
            @Parameter(description = "limit the number of records returned in one page")
            @RequestParam(value = "size", defaultValue = "20") @Min(1) Integer size,
            @Parameter(description = "sort the fetched data in either ascending (asc) " +
                    "or descending (desc) according to one or more of the person " +
                    "properties. Eg. to sort the list in ascending order base on the " +
                    "firstName property; the value will be set to firstName.asc")
            @RequestParam(value = "sort", defaultValue = "firstName.asc") List<String> sorts)
    {
        return personService.getPersons(
                PageRequest.of(page, size, Sort.by(CerebrumControllerUtilities.getOrders(sorts))));
    }

    /* get person */
    @Operation(summary = "find person by UUID", description = "Returns a detailed person information corresponding to the UUID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "successful operation",
                    content = @Content(schema = @Schema(implementation = Person.class))),
            @ApiResponse(responseCode = "400", description = "invalid person UUID supplied",
                    content = @Content(schema = @Schema(implementation = CerebrumApiError.class))),
            @ApiResponse(responseCode = "404", description = "person not found",
                    content = @Content(schema = @Schema(implementation = CerebrumApiError.class)))
    })
    @GetMapping(path = "/{uuid}")
    public Person getPerson(
            @Parameter(description = "UUID of the person that needs to be fetched")
            @PathVariable() String uuid)
    {
        return personService.getPerson(uuid);
    }

    /* create a person */
    @PreAuthorize("isAuthenticated()")
    @Operation(summary = "add a new person",
            security = @SecurityRequirement(name = "hdf-aai"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "person created",
                    content = @Content(schema = @Schema(implementation = Person.class))),
            @ApiResponse(responseCode = "400", description = "invalid UUID supplied",
                    content = @Content(schema = @Schema(implementation = CerebrumApiError.class))),
            @ApiResponse(responseCode = "401", description = "unauthorised", content = @Content()),
            @ApiResponse(responseCode = "403", description = "forbidden", content = @Content())
    })
    @PostMapping(path = "", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Person> createPerson(
            @Parameter(description = "Person object that needs to be added to the marketplace",
                    required = true, schema = @Schema(implementation = Person.class))
            @Valid @RequestBody Person person, UriComponentsBuilder uriComponentsBuilder)
    {
        return personService.createPerson(person, uriComponentsBuilder);
    }

    /* update person */
    @PreAuthorize("isAuthenticated()")
    @Operation(summary = "update an existing person",
            description = "Update part (or all) of a person information",
            security = @SecurityRequirement(name = "hdf-aai"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation",
                    content = @Content(schema = @Schema(implementation = Person.class))),
            @ApiResponse(responseCode = "201", description = "person created",
                    content = @Content(schema = @Schema(implementation = Person.class))),
            @ApiResponse(responseCode = "400", description = "invalid UUID supplied",
                    content = @Content(schema = @Schema(implementation = CerebrumApiError.class))),
            @ApiResponse(responseCode = "401", description = "unauthorised", content = @Content()),
            @ApiResponse(responseCode = "403", description = "forbidden", content = @Content())
    })
    @PutMapping(path = "/{uuid}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Person> updatePers(
            @Parameter(
                    description = "Person to update or replace. This cannot be null or empty.",
                    schema = @Schema(implementation = Person.class),
                    required = true) @Valid @RequestBody Person person,
            @Parameter(description = "UUID of the person that needs to be updated")
            @PathVariable() String uuid, UriComponentsBuilder uriComponentsBuilder)
    {
        return personService.updatePerson(uuid, person, uriComponentsBuilder);
    }

    /* JSON PATCH person */
    @PreAuthorize("isAuthenticated()")
    @Operation(summary = "partially update an existing person",
            security = @SecurityRequirement(name = "hdf-aai"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation",
                    content = @Content(schema = @Schema(implementation = Person.class))),
            @ApiResponse(responseCode = "400", description = "invalid UUID or json patch body",
                    content = @Content(schema = @Schema(implementation = CerebrumApiError.class))),
            @ApiResponse(responseCode = "401", description = "unauthorised", content = @Content()),
            @ApiResponse(responseCode = "403", description = "forbidden", content = @Content()),
            @ApiResponse(responseCode = "404", description = "person not found",
                    content = @Content(schema = @Schema(implementation = CerebrumApiError.class))),
            @ApiResponse(responseCode = "500", description = "internal server error",
                    content = @Content(schema = @Schema(implementation = CerebrumApiError.class)))
    })
    @PatchMapping(path = "/{uuid}", consumes = "application/json-patch+json")
    public ResponseEntity<Person> partialUpdatePerson(
            @Parameter(description = "JSON Patch document structured as a JSON " +
                    "array of objects where each object contains one of the six " +
                    "JSON Patch operations: add, remove, replace, move, copy, and test",
                    schema = @Schema(implementation = JsonPatch.class),
                    required = true) @Valid @RequestBody JsonPatch patch,
            @Parameter(description = "UUID of the person that needs to be partially updated")
            @PathVariable() String uuid)
    {
        return personService.partiallyUpdatePerson(uuid, patch);
    }

    /* delete person */
    @PreAuthorize("isAuthenticated()")
    @Operation(summary = "deletes a person",
            description = "Removes the record of the specified " +
                    "person UUID from the database. The person " +
                    "unique identification number cannot be null or empty",
            security = @SecurityRequirement(name = "hdf-aai"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "successful operation", content = @Content()),
            @ApiResponse(responseCode = "401", description = "unauthorised", content = @Content()),
            @ApiResponse(responseCode = "403", description = "forbidden", content = @Content()),
    })
    @DeleteMapping(path = "/{uuid}")
    public ResponseEntity<Person> deletePerson(
            @Parameter(description = "person UUID to delete", required = true)
            @PathVariable(name = "uuid") String uuid)
    {
        return personService.deletePerson(uuid);
    }
}
