package de.helmholtz.cloud.cerebrum.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatch;
import lombok.SneakyThrows;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import de.helmholtz.cloud.cerebrum.entity.MarketUser;
import de.helmholtz.cloud.cerebrum.repository.MarketUserRepository;
import de.helmholtz.cloud.cerebrum.service.common.CerebrumServiceBase;
import de.helmholtz.cloud.cerebrum.service.common.ForeignKeyExecutorService;

@Service
public class MarketUserService extends CerebrumServiceBase<MarketUser, MarketUserRepository, ForeignKeyExecutorService>
{
    private final MarketUserRepository marketUserRepository;
    private final ForeignKeyExecutorService foreignKeyExecutorService;

    public MarketUserService(MarketUserRepository marketUserRepository,
                             ForeignKeyExecutorService foreignKeyExecutorService)
    {
        super(MarketUser.class, MarketUserRepository.class, ForeignKeyExecutorService.class);
        this.marketUserRepository = marketUserRepository;
        this.foreignKeyExecutorService = foreignKeyExecutorService;
    }

    public Page<MarketUser> getUsers(PageRequest page)
    {
        return getAllEntities(page, marketUserRepository);
    }

    public MarketUser getUser(String uuid)
    {
        return getEntity(uuid, marketUserRepository);
    }

    public MarketUser getUser(JsonNode user)
    {
        //sub
        return marketUserRepository.findBySub(user.get("sub").asText());
    }

    public MarketUser getUserByAttributes(String attr, String value)
    {
        return getEntity(attr, value, marketUserRepository);
    }

    public MarketUser createUser(MarketUser entity)
    {
        return createEntity(entity, marketUserRepository, foreignKeyExecutorService);
    }

    public ResponseEntity<MarketUser> createUser(
            MarketUser entity, UriComponentsBuilder uriComponentsBuilder)
    {
        return createEntity(entity, marketUserRepository, foreignKeyExecutorService, uriComponentsBuilder);
    }

    public ResponseEntity<MarketUser> updateUser(
            String uuid, MarketUser entity, UriComponentsBuilder uriComponentsBuilder)
    {
        return updateEntity(uuid, entity, marketUserRepository, foreignKeyExecutorService, uriComponentsBuilder);
    }

    public ResponseEntity<MarketUser> partiallyUpdateUser(String uuid, JsonPatch patch)
    {
        return partiallyUpdateEntity(uuid, marketUserRepository, foreignKeyExecutorService, patch);
    }

    public ResponseEntity<MarketUser> deleteUser(String uuid)
    {
        return deleteEntity(uuid, marketUserRepository, foreignKeyExecutorService);
    }

    //affiliation
    public ResponseEntity<MarketUser> addAffiliation(String userUuid, String affiliationUuid)
    {
        return affiliation(userUuid, affiliationUuid, true);
    }

    public ResponseEntity<MarketUser> deleteAffiliation(String userUuid, String affiliationUuid)
    {
        return affiliation(userUuid, affiliationUuid, false);
    }

    @SneakyThrows
    private ResponseEntity<MarketUser> affiliation(String userUuid, String affiliationUuid, boolean toAdd)
    {
        MarketUser retrievedUser = getUser(userUuid);
        ObjectMapper objectMapper = new ObjectMapper();
        MarketUser submittedUser = objectMapper
                .readValue(objectMapper.writeValueAsString(retrievedUser), MarketUser.class);

        if (toAdd) {
            if (retrievedUser.getAffiliations().contains(affiliationUuid)) return ResponseEntity.noContent().build();
            submittedUser.addAffiliation(affiliationUuid);
        } else {
            if (!retrievedUser.getAffiliations().contains(affiliationUuid)) return ResponseEntity.noContent().build();
            submittedUser.removeAffiliation(affiliationUuid);
        }
        foreignKeyExecutorService.executeBeforeUpdate(retrievedUser, submittedUser);
        MarketUser updatedUser = marketUserRepository.save(submittedUser);
        return ResponseEntity.ok().body(updatedUser);
    }
}
