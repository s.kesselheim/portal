package de.helmholtz.cloud.cerebrum.errorhandling;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class CerebrumExceptionHandlerTest
{
    private static final String API_URI_PREFIX = "/api/v0";
    @Value("${cerebrum.test.oauth2-token}") private String TOKEN;
    @Autowired private MockMvc mockMvc;
    @Autowired private ObjectMapper objectMapper;

    @Test void
    whenTry_thenOK() throws Exception
    {
        final MvcResult response = mockMvc.perform(get(API_URI_PREFIX))
                .andExpect(status().isOk())
                .andReturn();
        System.out.println(response.getResponse().getContentAsString());
    }

    // handleNoHandlerFoundException
    @Test void
    whenNoHandlerForHttpRequest_thenNotFound() throws Exception
    {
        final MvcResult response = mockMvc.perform(delete(API_URI_PREFIX + "/xx")
                .header("Authorization", "Bearer " + TOKEN))
                .andExpect(status().isNotFound())
                .andReturn();
        final CerebrumApiError error = objectMapper.readValue(
                response.getResponse().getContentAsString(), CerebrumApiError.class);
        assertEquals(HttpStatus.NOT_FOUND, error.getStatus());
        assertEquals(1, error.getErrors().size());
        assertTrue(error.getErrors().get(0).contains("No handler found"));
        System.out.println(response.getResponse().getContentAsString());
    }

    //handleAccessDeniedException
    @Test void
    whenAccessDeniedException_thenForbidden() throws Exception
    {
        final MvcResult response =
                mockMvc.perform(get(API_URI_PREFIX + "/users/whoami")
                        .accept("application/json"))
                        .andExpect(status().isForbidden())
                        .andReturn();

        assertEquals(403, response.getResponse().getStatus());
    }
}
