package de.helmholtz.cloud.cerebrum;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import de.helmholtz.cloud.cerebrum.controller.MarketServiceController;
import de.helmholtz.cloud.cerebrum.controller.MarketUserController;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class HelmholtzCerebrumApplicationTest
{
    @Autowired private MarketUserController marketUserController;
    @Autowired private MarketServiceController marketServiceController;

    @Test
    void contextLoads()
    {
        assertThat(marketUserController).isNotNull();
        assertThat(marketServiceController).isNotNull();
    }
}