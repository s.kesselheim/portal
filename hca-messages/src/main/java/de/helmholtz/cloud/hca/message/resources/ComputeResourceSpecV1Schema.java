package de.helmholtz.cloud.hca.message.resources;

import javax.annotation.processing.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import de.helmholtz.cloud.hca.message.AllocateResourceSpecification;


/**
 * ComputeResourceSpecV1
 * <p>
 * 
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "cpu",
    "ram",
    "storage"
})
@Generated("jsonschema2pojo")
public class ComputeResourceSpecV1Schema extends AllocateResourceSpecification {

    /**
     * QuotaV1
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("cpu")
    private QuotaV1Schema cpu;
    /**
     * QuotaV1
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("ram")
    private QuotaV1Schema ram;
    /**
     * QuotaV1
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("storage")
    private QuotaV1Schema storage;

    /**
     * QuotaV1
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("cpu")
    public QuotaV1Schema getCpu() {
        return cpu;
    }

    /**
     * QuotaV1
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("cpu")
    public void setCpu(QuotaV1Schema cpu) {
        this.cpu = cpu;
    }

    /**
     * QuotaV1
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("ram")
    public QuotaV1Schema getRam() {
        return ram;
    }

    /**
     * QuotaV1
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("ram")
    public void setRam(QuotaV1Schema ram) {
        this.ram = ram;
    }

    /**
     * QuotaV1
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("storage")
    public QuotaV1Schema getStorage() {
        return storage;
    }

    /**
     * QuotaV1
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("storage")
    public void setStorage(QuotaV1Schema storage) {
        this.storage = storage;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(ComputeResourceSpecV1Schema.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("cpu");
        sb.append('=');
        sb.append(((this.cpu == null)?"<null>":this.cpu));
        sb.append(',');
        sb.append("ram");
        sb.append('=');
        sb.append(((this.ram == null)?"<null>":this.ram));
        sb.append(',');
        sb.append("storage");
        sb.append('=');
        sb.append(((this.storage == null)?"<null>":this.storage));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.cpu == null)? 0 :this.cpu.hashCode()));
        result = ((result* 31)+((this.storage == null)? 0 :this.storage.hashCode()));
        result = ((result* 31)+((this.ram == null)? 0 :this.ram.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ComputeResourceSpecV1Schema) == false) {
            return false;
        }
        ComputeResourceSpecV1Schema rhs = ((ComputeResourceSpecV1Schema) other);
        return ((((this.cpu == rhs.cpu)||((this.cpu!= null)&&this.cpu.equals(rhs.cpu)))&&((this.storage == rhs.storage)||((this.storage!= null)&&this.storage.equals(rhs.storage))))&&((this.ram == rhs.ram)||((this.ram!= null)&&this.ram.equals(rhs.ram))));
    }

}
