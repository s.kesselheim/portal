*Service Name:*

Unique name of the service

`e.g.: bwSync&Share`

---

*Software Name:*

Name of the software product on which the service is based

`e.g.: Nextcloud`

---

*Short text (in cards):*

Short desciption about what this service 

`e.g.: File Sync and Share, Groupware-Functionalities: Files, Fotos, Calendar, Contacts, Deck.`

---

*Long text:*

Long description about this services. Here you can describe
- what this service is about
- what the functions of the service are, 
- links to websites with further information about the service and 
- how the users can get access to the service

`e.g.: bwSync&Share enables users to synchronize or exchange their data between different computers and mobile devices. In addition to the hardware-independent web interface, different clients are available (Windows and Mac desktop, apps for Android and iPhone/iPad). With that, bwSync&Share is a powerful service for synchronizing and sharing desktop data. Find tips in our [FAQ](https://help.bwsyncandshare.kit.edu/126.php), first steps are described in the quick guide. In addition, the Service Description and Deprovisioning points inform you about how the service works.
bwSync&Share offers the possibility to access data jointly and to exchange it with other users or to edit it together. By inviting to a folder, a personal storage quota is made available to the invited party in accordance with the granted authorization. You can also send links to download files or folders.
It has been operated at the Karlsruhe Institute of Technology (KIT) since January 1, 2014. Information about the  service can be found [here](https://help.bwsyncandshare.kit.edu/126.php).`

---

*User Enablement:*

Describe how do users get access to the service. Possible options could be:
- No preconditions are necessary
- User have to member of a VO (virtual organization, handlet by Unity)
- User have to meet preconditions like a special entitlement to gain access to the service

`e.g.:
To use the service, you need to be member of the VO "XYZ" which applies for service usage. To get access to that VO please contact contact@service`

OR

`e.g.:
**Precondition to use the service:**  

Currently, only users of Helmholtz centres whose Identity Providers (IdP) provide the correct entitlement can access this service.
Access will only be possible to users of centres, which IdP transmits this entitlement.`

OR 

`e.g.:
Login via the Helmholtz AAI / Gitlab. Your account will automatically be provisioned.`

---

*Contact for user support:*

Contact address, preferably a mailinglist

`e.g.: servicedesk@scc.kit.edu`

---

*Entrypoint:*

URL which points the user directly to your service



`e.g.: [https://bwsyncandshare.kit.edu/](https://bwsyncandshare.kit.edu/)`
