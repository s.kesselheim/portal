package de.helmholtz.cloud.hca.service;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.helmholtz.cloud.hca.entity.HCARequest;
import de.helmholtz.cloud.hca.message.ResourceCreatedV1Schema;
import de.helmholtz.cloud.hca.repository.HCARequestRepository;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class Receiver {
    @Autowired
    private HCARequestRepository repository;

    /* listens for new messages from HCA and calls handlers according to type */
    @RabbitListener(queues = "${hca.receiver.queue}")
    public void listen(Message message) {
        String messageType = message.getMessageProperties().getType();
        log.info("Got a new message");
        log.debug(String.format("Message type is: %s", messageType));
        log.debug(String.format("Message ID is: %s", message.getMessageProperties().getMessageId()));
        log.debug(String.format("Message body: %s", new String(message.getBody())));
        if (messageType.equals("ResourceCreatedV1")) {
            handleResourceCreated(message);
        } else {
            log.warn(String.format("Got unsupported message type: %s", messageType));
        }
    }

    /*
     * handles ResourceCreated messages type, i.e., updates the status and fills the
     * resourceId provided by the service
     */
    public void handleResourceCreated(Message message) {
        log.info("Handling ResourceCreatedV1");
        try {
            ObjectMapper Obj = new ObjectMapper();
            String className = String.format("de.helmholtz.cloud.hca.message.%sSchema",
                    message.getMessageProperties().getType());
            ResourceCreatedV1Schema response = (ResourceCreatedV1Schema) Obj.readValue(message.getBody(),
                    Class.forName(className));

            HCARequest request = repository.findByRequestId(message.getMessageProperties().getMessageId()).get(0);
            if (request == null) {
                log.error(String.format("Cannot find request with ID: %s",
                        message.getMessageProperties().getMessageId()));
                return;
            }
            request.setStatus("done");
            request.setResourceId(response.getId());
            repository.save(request);
            log.info(String.format("Request %s was updated", request.getRequestId()));
        } catch (IOException e) {
            log.error("Cannot decode message: " + e);
        } catch (ClassNotFoundException e) {
            log.error("Cannot decode message: " + e);
        }
    }
}
