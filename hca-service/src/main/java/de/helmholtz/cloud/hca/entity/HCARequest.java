package de.helmholtz.cloud.hca.entity;

import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import de.helmholtz.cloud.hca.message.ResourceAllocateV1Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
@Document
public class HCARequest extends AuditMetadata {
    @Setter(AccessLevel.NONE)
    @Id
    private String uuid;

    @Version
    private Long version;

    @NonNull
    private ResourceAllocateV1Schema request;

    @NonNull
    private String status;

    @NotNull
    private String requestId;

    @NotNull
    private String userId;

    @NotNull
    private String serviceName;

    @Nullable
    private String resourceId;

    public HCARequest() {
    }
}
