import { LitElement, html, css } from 'lit-element';

class HelmholtzCloudFooter extends LitElement
{
    static get properties()
    {
        return {
            minima: {
                type: Boolean,
                attribute: true
            },
            backgroundColor: {
                type: Boolean,
                attribute: true
            }
        };
    }

    static get styles()
    {
        return css`
            :host {
                display: block;
                width: 100%;
                min-width: 320px;
                height: 100%;
                color: #005aa0;
                background-color: #fff;
            }
            :host([backgroundcolor]) {
                background-color: #005aa0;
                color: #fff;
            }
            * {
                box-sizing: border-box;
            }
            img {
                max-width: 100%;
                max-width: 250px;
            }
            .inner-footer {
                display: flex;
                width: 100%;
                height: 100%;
                flex-wrap: wrap;
            }
            .no-display {
                display: none;
            }
            .helmholtz-logo-container {
                padding-top: 10px;
                padding-left: 2%;
            }
            nav {
                flex: 1 1 auto;
                display: flex;
                flex-wrap: wrap;
                justify-content: center;
                align-items: center;
                color: #333;
                padding-top: 10px;
                padding-left: 10px;
            }
            :host([backgroundcolor]) nav {
                color: #fff;
            }
            nav a {
                text-decoration: none;
                color: #333;
                font-weight: 400;
                font-size: 0.8em;
                cursor: pointer;
            }
            :host([backgroundcolor]) nav a {
                color: #ccc;
            }
            a + a {
                padding-left: 10px;
            }
            .decl {
                min-width: 170px;
            }
            @media screen and (min-width: 700px) {
                a + a {
                    padding-left: 5%;
                }
            }
            @media screen and (min-width: 900px) {
                nav {
                    padding-right: 20px;
                }
                :host([minima]) nav {
                    justify-content: center;
                }
                :host nav {
                    justify-content: flex-end;
                }
            }
        `;
    }

    render()
    {
        return html`
            <div class="inner-footer">
                ${this.minima ? ``:
                    html`
                        <div class="helmholtz-logo-container">
                            <img src=${this.backgroundColor ?
                                `media/i/Helmholtz_white.svg` :
                                `media/i/helmholtz_blue.svg`}>
                        </div>
                    `
                }
                <nav>
                    <a href="https://hifis.net/contact.html">Helpdesk</a>
                    <a href="https://www.desy.de/imprint">Imprint</a>
                    <a href="https://www.desy.de/data_privacy_policy">Privacy</a>
                    <a class="decl" 
                        href="https://www.desy.de/declaration_of_accessibility/index_eng.html">
                        Declaration of Accessibility</a>
                </nav>
            </div>
        `;
    }
}
customElements.define('helmholtz-cloud-footer', HelmholtzCloudFooter);
