import { LitElement, html, css } from 'lit-element';

import '@material/mwc-button';

class NotificationBanner extends LitElement
{
    constructor()
    {
        super();
        this.message = "This is a preliminary version of the cloud portal. " +
            "We're working on making it even better. Come back soon to see what's new!";
        this.page = "landing";
    }
    static get properties()
    {
        return {
            message: {
                type: String,
            },
            page: {
                type: String,
            }
        };
    }
    static get styles()
    {
        return css`
            :host {
                width: 100%;
                display: block;
                padding: 10px;
                box-sizing: border-box;
            }
            * {
                box-sizing: border-box;
            }
            .container {
                display: flex;
                align-items: center;
                flex-direction: column;
            }
            .information {
                flex: 1 1 auto;
            }
            .button {
                display: flex;
                width: 100%
            }
            .flex {
                flex: 1 1 auto;
            }
            mwc-button {
                --mdc-theme-primary: #005aa0;
            }
            @media screen and (min-width: 700px) {
                .container {
                    width: 700px;
                    margin: auto;
                }
            }
        `;
    }
    render()
    {
        return html`
            <div class="container">
                <div class="information">${this._returnString(this.message)}</div>
                <div class="button">
                    <div class="flex"></div><mwc-button @click="${this._dismiss}" label="Got it"></mwc-button>
                </div>
            </div>
        `;
    }
    _dismiss()
    {
        this.dispatchEvent(new CustomEvent(`helmholtz-cloud-dismiss-notification-${this.page}`, {
            detail: {dismiss: true}, bubbles: true, composed: true}));
        window.localStorage.setItem(`notification-${this.page}`, 'dismiss');
    }
    _returnString(str)
    {
        return document.createRange().createContextualFragment(`${ str }`);
    }
}
customElements.define('notification-banner', NotificationBanner);